import { Col, Row } from "..";

const Home = () => {
  return (
    <>
      <h1>Página Inicial</h1>
      <Row>
        <Col>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium necessitatibus nemo
            distinctio harum earum explicabo illum magnam neque ab quia laboriosam, natus enim dolor
            nisi itaque nostrum dolorem nihil tempore autem? Aliquam hic repellendus ea nobis
            repudiandae voluptatem rem ullam labore sapiente. Sunt eveniet minima quis alias, id
            dolorum? Quam.
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus mollitia amet, ratione
            doloribus accusamus optio possimus! Vel dicta minus quam dolor quas dolore ullam quos
            amet non quae nostrum, doloribus iste, explicabo dolorem eaque sapiente voluptates, iure
            illo dolores recusandae nobis. Labore quo perspiciatis nihil inventore facilis eos,
            repudiandae fugiat!
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium similique, dolorum
            soluta rerum eius veritatis, deserunt delectus necessitatibus sit dicta quisquam sunt
            explicabo? Sint cupiditate iste architecto similique corrupti iure reprehenderit non
            quam reiciendis, soluta officiis eveniet quasi at, possimus mollitia. Libero, ullam?
            Dicta veniam mollitia aperiam similique possimus officia!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus adipisci aliquid
            maxime dolore autem. Unde libero fuga atque, consectetur animi natus deserunt sunt
            repellendus at illum ipsum nostrum vel beatae. Quam iste cupiditate adipisci non
            consequuntur aspernatur quisquam nisi consectetur recusandae repellendus porro soluta
            neque, labore consequatur? Quod, consequatur sapiente?
          </p>
        </Col>
        <Col>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium necessitatibus nemo
            distinctio harum earum explicabo illum magnam neque ab quia laboriosam, natus enim dolor
            nisi itaque nostrum dolorem nihil tempore autem? Aliquam hic repellendus ea nobis
            repudiandae voluptatem rem ullam labore sapiente. Sunt eveniet minima quis alias, id
            dolorum? Quam.
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus mollitia amet, ratione
            doloribus accusamus optio possimus! Vel dicta minus quam dolor quas dolore ullam quos
            amet non quae nostrum, doloribus iste, explicabo dolorem eaque sapiente voluptates, iure
            illo dolores recusandae nobis. Labore quo perspiciatis nihil inventore facilis eos,
            repudiandae fugiat!
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium similique, dolorum
            soluta rerum eius veritatis, deserunt delectus necessitatibus sit dicta quisquam sunt
            explicabo? Sint cupiditate iste architecto similique corrupti iure reprehenderit non
            quam reiciendis, soluta officiis eveniet quasi at, possimus mollitia. Libero, ullam?
            Dicta veniam mollitia aperiam similique possimus officia!
          </p>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus adipisci aliquid
            maxime dolore autem. Unde libero fuga atque, consectetur animi natus deserunt sunt
            repellendus at illum ipsum nostrum vel beatae. Quam iste cupiditate adipisci non
            consequuntur aspernatur quisquam nisi consectetur recusandae repellendus porro soluta
            neque, labore consequatur? Quod, consequatur sapiente?
          </p>
        </Col>
      </Row>
    </>
  );
};

export default Home;
